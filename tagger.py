# -*- coding: utf-8 -*-

import sys
import re
import io
from vedavaapi.ashtadhyayi.subanta import Subanta
from indic_transliteration import sanscript

def get_corpus_text(file_path):
    with io.open(file_path, "r", encoding='utf-8') as corpus_file:
        return corpus_file.read()

def tokenize(text):
    return text.split(" ")

def is_tagged(token):
    if re.match(r"^.+\((\w)*v(\d)*#(\d)*\)$", token):
        return True
    else: 
        return False

def get_tag_data(token):
    pada, tag = tuple(token.strip(")").split("("))
    qualifier, tag = tuple(tag.split("l"))
    linga_letter, tag = tuple(tag.split("v"))
    vibhakti, vachana = tuple(tag.split("#"))
    letter_linga_map = {"p" : "puM", "s" : "strI", "n": "napuM"}
    linga = letter_linga_map[linga_letter]
    return {'pada': pada, 'qualifier': qualifier, 'linga': linga, 'vibhakti': int(vibhakti), 'vachana': int(vachana)}

def get_praatipadikam(pada, vibhakti, vachana):
    praatipadikam_data = Subanta.analyze({'pada': pada, 'vibhakti': vibhakti, 'vachana': vachana})[0]
    praatipadikam = praatipadikam_data['praatipadikam']
    return sanscript.transliterate(praatipadikam, sanscript.SLP1, sanscript.DEVANAGARI)

def get_stem(pada, vibhakti, vachana):
    praatipadikam_data = Subanta.analyze({'pada': pada, 'vibhakti': vibhakti, 'vachana': vachana})[0]
    stem = praatipadikam_data['stem']
    return stem

def get_subanta(qualifier, stem, praatipadikam, linga):
    linga_letter_map = {"puM": "p", "strI": "s", "napuM": "n"}
    linga_letter = linga_letter_map[linga]
    subanta = {}
    praatipadikam = sanscript.transliterate(praatipadikam, sanscript.DEVANAGARI, sanscript.SLP1)
    for suffixes in Subanta.vibhakti_suffixes:
        if suffixes['linga'] == linga and suffixes['anta'] == praatipadikam[-1]:
            vibhakti_count = 0
            for suffix_list in suffixes['vibhaktis']:
                vachana_count = 0
                vibhakti_count += 1
                for suffix in suffix_list:
                    vachana_count += 1
                    if "/" in suffix:
                        suffix_alternatives = suffix.split("/")
                        for suffix_alt in suffix_alternatives:
                            subanta_item = sanscript.transliterate(stem + suffix_alt, sanscript.SLP1, sanscript.DEVANAGARI)
                            if subanta_item not in subanta:
                                subanta[subanta_item] = qualifier + "l" + linga_letter + "v" + str(vibhakti_count) + "#" + str(vachana_count)
                    else:
                        subanta_item = sanscript.transliterate(stem + suffix, sanscript.SLP1, sanscript.DEVANAGARI)
                        if subanta_item not in subanta:
                            subanta[subanta_item] = qualifier + "l" + linga_letter + "v" + str(vibhakti_count) + "#" + str(vachana_count)
    return subanta

corpus_file_path = sys.argv[1]
tagged_corpus_file_path = sys.argv[2]
corpus_text = get_corpus_text(corpus_file_path)
tokens = tokenize(corpus_text)

for token in tokens:
    token = token.strip()
    if is_tagged(token):
        tag_data = get_tag_data(token)
        praatipadikam = get_praatipadikam(tag_data["pada"], tag_data["vibhakti"], tag_data["vachana"])
        stem = get_stem(tag_data["pada"], tag_data["vibhakti"], tag_data["vachana"])
        subanta = get_subanta(tag_data["qualifier"], stem, praatipadikam, tag_data["linga"])
        for token in tokens:
            if not is_tagged(token) and praatipadikam in token and token in subanta:
                token_with_tag = "%s(%s)" % (token, subanta[token])
                print(token)
                print(token_with_tag)
                corpus_text = corpus_text.replace(token, token_with_tag)

with io.open(tagged_corpus_file_path, 'w', encoding='utf-8') as tagged_corpus_file:
  tagged_corpus_file.write(corpus_text)
