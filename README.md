Subanta Tagging
===============
Using this Python script, if one of the vibhakti forms of a pada is tagged in a Sanskrit corpus, all other vibhakthi forms of the pada are automatically tagged using this script.

## How to use
1. Clone the ashtadhyayi project using, `git clone https://github.com/vedavaapi/ashtadhyayi`

2. Change directory to ashtadhyayi using, `cd ashtadhyayi` and install the package using, `python setup.py install`. 
    
    Note: If you want to install the package locally, make sure you activate the virtualenv before this step.

3. Run `python tagger.py <path-to-corpus> <path-to-tagged-corpus>` from the root directory of this project.
   
   Note: If you are using a virtualenv, make sure you activate the virtualenv before this step.